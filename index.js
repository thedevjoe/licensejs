const axios = require('axios');
const { exec } = require('child_process');
const fs = require('fs');
const path = require('path');
const homeBase = 'https://license.devjoe.net';
const jwt = require('jsonwebtoken');

let projectId = null;
let pubKey = null;
let config = null;

function p(path) {
    if(path.startsWith('/')) return homeBase + path;
    else return homeBase + "/" + path;
}

/*function currentHash() {
    return new Promise((res, rej) => {
        exec('git show-ref -s refs/heads/master', (err, stdout, stderr) => {
            if(stderr.indexOf('fatal') !== -1) return res(null);
            res(stdout.trim());
        })
    });
}

function updateCheck(doup) {
    return new Promise((res, rej) => {
        if(!doup) {
            return res();
        }
        currentHash().then((e) => {
            var vpath = path.join(__dirname, 'version.dj');
            var version = e;
            if(e == null) {
                if(!fs.existsSync(vpath)) {
                    fs.writeFileSync(vpath, projectData.update);
                    console.log('LJS: VCS was not found. A version file was created to track the current version.');
                }
                version = fs.readFileSync(vpath);
            }
            if(version != projectData.update) {
                console.log('LJS: Update was found. Current hash is ' + version + '. Latest hash is ' + projectData.update + ".");
                if(e != null) {
                    console.log("Attempting autoupdate with git.");
                    exec("git pull origin master");
                }
                if(updateFunc) updateFunc(version, projectData.update);
            } else console.log('LJS: Up to date.');
            res();
        })
    });
}*/ // Deprecated since 2020-05

var valid_license = null;

function offlineVerify(license) {
    return new Promise((res, rej) => {
        let pl = null;
        try {
            pl = jwt.verify(license, pubKey, {algorithms: ["RS256"], audience: projectId});
        } catch(e) {
            console.error(e);
            return res({success: false, error: "Invalid Signature"});
        }
        axios.get(p('/lrl')).then(e => {
            let lrl = e.data;
            let listing = lrl.find(e => e.id === pl.license.id);
            if(listing != null) return res({success: false, error: "License Revoked"});
            res({success: true, data: pl});
        });
    });
}

function validateLicense(license = null, pullConfig = false) {
    return new Promise((res, rej) => {
        if(license == null) {
            var licenseFile = 'license.dj';
            if(!fs.existsSync(licenseFile)) {
                console.log('LJS: Creating a new license');
                axios.post(p('/license/' + projectId)).then((e) => {
                    fs.writeFileSync(licenseFile, e.data.data);
                    console.log('LJS: Activated new license (' + e.data.data + ')');
                    validateLicense().then(() => res());
                });
                return;
            }
            license = fs.readFileSync(licenseFile).toString('utf8');
        }
        offlineVerify(license).then(({success, data, error}) => {
            if(!success) {
                console.error('LJS FATAL: LICENSE IS INVALID. REFUSING TO CONTINUE. (' + error + ")");
                process.exit(1);
                return;
            }
            valid_license = license;
            if(pullConfig)
                axios.post(p('/license/verify/'), {license}).then((e) => {
                    var data = e.data;
                    if(data.code !== 200) {
                        console.error('LJS FATAL: LICENSE IS INVALID. REFUSING TO CONTINUE.');
                        process.exit(1);
                        return;
                    }
                    console.log('LJS: License verified.');
                    config = data.data.config;
                    res();
                });
            else {
                console.log('LJS: License verified.');
                res();
            }
        });
    });
}

var updateFunc = null;

function pushEvent(event) {
    axios.put(p('/analytics'), {license: valid_license, event}).then((e) => {
        if(!module.exports.eventDebug) return;
        console.log('LJS: Pushed event ' + event + ' successfully.');
    });
}

//var offlineData = {};

//if(fs.existsSync(path.join(__dirname, 'offlineData'))) offlineData = JSON.parse(fs.readFileSync(path.join(__dirname, 'offlineData')).toString());

/*function saveOffline() {
    var savePath = path.join(__dirname, 'offlineData');
    fs.writeFileSync(savePath, JSON.stringify(offlineData));
}

function getData(field) {
    return new Promise((res, rej) => {
        if(module.exports.offline) {
            return res(offlineData[field]);
        }
        axios.get(p('/data/' + encodeURIComponent(field)), {
            headers: {
                'X-License': valid_license,
                'X-Project': projectData.id
            }
        }).then((e) => {
            res(e.data.data.value);
        });
    });
}

function setData(field, value) {
    return new Promise((res, rej) => {
        if(module.exports.offline) {
            offlineData[field] = value;
            saveOffline();
            res();
            return;
        }
        getData(field).then((e) => {
            if(e == null) {
                axios.put(p('/data/' + encodeURIComponent(field)), {value}, {
                    headers: {
                        'X-License': valid_license,
                        'X-Project': projectData.id
                    }
                }).then(() => res());
            }
            else {
                axios.patch(p('/data/' + encodeURIComponent(field)), {value}, {
                    headers: {
                        'X-License': valid_license,
                        'X-Project': projectData.id
                    }
                }).then(() => res());
            }
        });
    });

}*/ // Deprecated since 2020-01

/*function setUpdateFunction(func) {
    updateFunc = func;
}*/

function initialize(pid, options = {}) {
    let opt = {
        downloadConfig: false,
        license: null
    };
    Object.assign(opt, options);
    return new Promise((res, rej) => {
        projectId = pid;
        axios.get(p('/jwt/pubkey')).then(e => {
            console.log("LJS: Downloaded license.devjoe.net public key.");
            pubKey = e.data;
            validateLicense(opt.license, opt.downloadConfig).then(() => {
                res();
            });
        });
    });
}

/*function getInfo() {
    let clone = {};
    Object.assign(clone, projectData);
    return clone;
}*/

function getConfig() {
    return config;
}

module.exports = {initialize, getConfig, pushEvent, eventDebug: false};